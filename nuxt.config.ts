export default defineNuxtConfig({
  devtools: { enabled: false },
  nitro: {
    devProxy: {
      '/tmp/nitro/worker.sock': '/var/tmp/nitro/worker.sock'
    }
  },
});
